require_relative 'board.rb'
require_relative 'player.rb'

class BattleshipGame
  attr_reader :board, :player
  def initialize(player=HumanPlayer.new("John"),board)
    @player = player
    @board = board
  end

  def attack(pos)
    pos = pos.flatten
    puts "_______attack__________"
    board.grid[pos[0]][pos[1]] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    puts "________play_turn_______________"
    move = player.get_play
    attack(move)
  end

  def display_hack
    puts "#{board.grid}"
  end

  def display_player_board
    puts "#{board.player_grid}"
  end

  def setup
    board.place_random_ship
  end

  def play
    setup
    until game_over?
      play_turn
    end
    puts ""
    puts "Hit!"
    puts ""
  end
end

if __FILE__ == $PROGRAM_NAME
  board = Board.new
  game = BattleshipGame.new(board)
  game.play
end
